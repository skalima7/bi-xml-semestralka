# V1
Obsahuje více zanořenou xml strukturu, ale méně dat. Má složitější rozložení exportovaných stránek na tři sloupce. Exportované soubory obsahují obrázky.

### Features:
* export do EPUB|PDF|HTML
* validace RNC|DTD

# V2
Obsahuje html2xml parser napsaný v JavaScriptu, kterému stačí dát url adresu na zemi ve world-factbook a parser si ji stáhne a vytvoří z ní xml soubor do umístění `countries/{code}.xml`. Ve výchozím stavu se stahuje 6 zemí (17K řádek v xml).

### Features:
* export do PDF|HTML
* Makefile pro jednodušší ovládání
* validace RNC|DTD