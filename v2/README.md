# Semestrálka
Projekt se zabývá parsováním html stránek z world-factbook do XML souborů a jejich následným exportem do PDF a HTML. Navrhl jsem xml strukturu pro uložení informací o jednotlivách zemích a příslušné validační schéma DTD. Pro pokročilejčí validace používám RELAX NG s kompaktní syntaxí. Transformace do HTML|PDF jsou napsané pomocí XSL (XSLT a XSL-FO).

## html2xml
Parser je napsaný v jazyce JavaScript a dokáže parsovat libovolnou zemi. Stačí mu předat jako parametr danou url země z webu world-factbook. Výstupní xml soubor se uloží do `countries/{code}.xml`. Při psaní parseru jsem se snažil využít co nejvíce informací v html (primárně atributy class), abych získal co nejpodrobnější a strukturovaná data. Provádím konverze datumu a číslel do lépe manipulovatelných hodnot.

### Přidání vlastních zemí:
```bash
make crawl site=URL
```
> URL - např. `https://www.cia.gov/library/publications/the-world-factbook/geos/af.html`
      - lze jich předat více oddělených mezerou

## XML struktura
Element `country`, obsahuje název země v atributu title a má globální unikátní identifikátor code obsahující zkratku země jako "cz". `country` obsahuje elementy `section`, reprezentující hlavní sekce jako Geogprahy, Economy atd. - název je v atributu `title`, kterým jsou unikátně identifikovatelné v rámci jedné země. Každá sekce pak obsahuje elementy `field`, ty reprezentují menší části danné sekce např. "Unemployment rate" - opět atribute title funguje jako lokální identifikátor v rámci sekce. `Field` elementy obsahují `subfield`, které drží užitečné informace v elementech:
* `subfieldGroup` - název skupiny (všechny další `subfiled` patří do této skupiny, pokud neobsahují také `subfieldGroup`)
* `subfieldName` - Označení hodnoty
* `text`   - textová informace
* `subfieldNumber` - číselná informace s atributem unit
* `subfieldNote` - poznámka
* `subfieldDate` - časový údaj vzstahující se k datu pořízení informace

Přesná struktura je definovaná v `template.dtd`.

## Generování
Pomocí bash scriptu se vygeneruje soubor `all-dtd.xml`, který obsahuje načtení DTD definice a jednotlivých zemí ze složky `countries`. Pomocí xmlint ho zpracuji a výstup (xml obsahující spojené všechny země) uložím do `all.xml` nad kterým provádím transformace.

## HTML
Index obsahující odkazy na jednotlivé země a jejich sekce, nachází se v `output/index.html`, v dolní části je tabulka s pár zajímavostmi (zkoušel jsem si XPATH). Stránky s jednotlivými zeměmi jsou dělené na sekce - design je podobný jako ve world-factbook.

## PDF
Začátek dokumentu obsahuje index, který je generován automaticky s odkazy a používá odlišný layout stránky. V zápatí je vždy uvedeno číslo aktuální stránky. První stránky s názvem země obsahuje na pravé straně obrázek se znakem world-factbook.

## media
Složka je umístěna v `output/media` a obsahuje
* css - stylesheety pro formátování html
* images - obrázky
* js - soubory v jazyce javaScript použité v html

## Relax NG
Pro validace používám datové typy XSD. Z pokročilejších možností `patter` u stringu a pojmenování vzorů.

## Epub
Viz. [v1](../v1/README.md)

## TL;RD
```bash
make crawl site=https://www.cia.gov/library/publications/the-world-factbook/geos/af.html    # stažení a parsování země 'AFGHANISTAN' -> vytvoření countries/af.xml
make createAll  # vytvoří all-dtd.xml a all.xml
make validate   # spustí validace: RNG, RNC, DTD
make html   # Generování html: output/index.html
make pdf    # Generování pdf: output/out.pdf
make crawlDefault   # parsování výchozích 6 zemí

make    # provede všechny zmíněné příkazy výše
```