const fs = require("fs")
const jsdom = require("jsdom")
const fetch = require("node-fetch")
const path = require("path")
const { JSDOM } = jsdom;

const args = process.argv.slice(2)
if (args.length === 0) return console.log("Nothing to do...")

const promises = []
for (const url of args) {
    if (!url.startsWith("https://www.cia.gov/library/publications/the-world-factbook")) throw new Error('Invalid argument - only url to "https://www.cia.gov/library/publications/the-world-factbook" ALLOWED')

    promises.push(fetch(url).then(res => res.text()).then(body => ({ html: body, url })))
}

function prepare(text) {
    text = text.trim()
    if (text.endsWith(":")) text = text.slice(0, -1)
    // console.log(text)
    return text;
}

let log;

// xmlOpen("countries")
Promise.all(promises).then(values => {
    values.forEach(({ html, url }) => {
        console.log("Parsing:", url)
        const fileName = url.split("/").slice(-1)[0].replace(".html", ".xml")
        const wrStream = fs.createWriteStream(path.join(__dirname, "../countries", fileName))
        log = (text) => {
            text = prepare(text)
            wrStream.write(text + "\n")
        }

        const countryCode = fileName.replace(".xml", "")
        parseHTML(countryCode, html, url)
        wrStream.end()
    })
})
// xmlClose("countries")

function parseHTML(countryCode, html, url) {
    // const html = fs.readFileSync("South_Asia_Afghanistan-The_World_Factbook.html", 'utf8')
    const baseUrl = url.replace(/(?<=\/)[^/]+$/, "")
    const dom = new JSDOM(html, {
        url,
    })

    const window = dom.window
    const document = window.document
    const $ = document.querySelectorAll.bind(document)

    const anchors = $("li[id$='-section-anchor']")
    const sections = $("li[id$='-section']")
    const countryName = $(".region")[0].textContent


    xmlOpen("country", `title="${countryName}" code="${countryCode}"`)
    // for (const [id, item] of sections.entries()) {
    for (let id = 0; id < sections.length; id++) {
        // for (let id = 0; id < 2; id++) {
        const item = sections[id]
        const ref = anchors[id]

        // in browser getElementsByTagName returns collection with two "a" - second is right one
        const header = ref.getElementsByTagName("a")[0].children[0].firstChild
            .textContent.trim().replace(" ::", "")
        // console.log("h2", header)

        const xmlH2 = camelizeId(item.id.replace("-category-section", ""))
        // console.log("h2", xmlEl)
        xmlOpen("section", `class="${xmlH2}" title="${header}"`)

        const children = item.children  // první div je nadpis sekce, druhý samotný obsah
        for (let i = 0; i < children.length; i += 2) {
            const subHeader = children[i].firstElementChild.firstElementChild
                .innerHTML
            // console.log("h3", subHeader)

            const el = children[i + 1]  // TODO id=field-geographic-coordinates
            const sectionName = el.id.replace("field-", "")
            const xmlH3 = camelizeId(sectionName)
            // console.log("h3", xmlH3)
            xmlOpen("field", `title="${subHeader}"`)

            for (const data of el.children) {
                const groupedFileds = data.classList.contains("grouped_subfield");

                if (groupedFileds && [...data.childNodes].some(containsSlashText)) {
                    xmlOpen("groupedSubfield")
                    console.error("len", data.childNodes.length)
                    for (let j = 0; j < data.childNodes.length; j++) {
                        const isLast = j === data.childNodes.length - 1;
                        const it = data.childNodes[j]
                        // console.log("??", it.nodeName, it.textContent)
                        if (it.nodeName === "SPAN") {
                            const name = camelizeId(it.classList.item(0))
                            // xmlLog(name, it.innerHTML)
                            if (name === "subfieldName") xmlOpen("subfield")
                            parseSubField(name, it.innerHTML)
                        } else if (it.nodeName === "#text" || isLast) {
                            // ignore textNodes - just padding
                            if (it.textContent.trim() === "/" || isLast)
                                xmlClose("subfield")

                        } else console.error(`ERROR2> - not processing: ${it.nodeName}`)
                    }
                    xmlClose("groupedSubfield")
                    continue;
                }

                /**
                 * TOTAL CHAOS in html... Took really long time - 4 differente styles...
                 */
                if (data.classList.contains("note")) {
                    xmlOpen("notes")

                    if ([...data.childNodes].some(child => child.nodeName === "P")) {    // Contains P elements
                        for (const it of data.childNodes) {
                            if (it.textContent.trim() === "") continue
                            xmlOpen("note")
                            let closed = false;
                            for (const it2 of it.childNodes) {
                                if (it2.nodeName === "STRONG") {
                                    xmlLog("strong", it2.textContent)
                                    closed = false
                                }
                                else if (it2.nodeName === "BR" && !closed) {
                                    closed = true
                                    xmlClose("note")
                                    xmlOpen("note")
                                } else {
                                    closed = false
                                    xmlLog("text", it2.textContent)
                                }
                            }
                            xmlClose("note")
                        }
                    } else {
                        if (data.textContent.trim() !== "") {
                            xmlOpen("note")
                            let closed = false;
                            for (const it of data.childNodes) { // contains just one note

                                if (it.nodeName === "STRONG") {
                                    xmlLog("strong", it.textContent)
                                    closed = false
                                }
                                else if (it.nodeName === "BR" && !closed) {
                                    closed = true
                                    xmlClose("note")
                                    xmlOpen("note")
                                } else {
                                    closed = false
                                    xmlLog("text", it.textContent)
                                }
                            }
                            xmlClose("note")
                        }
                    }
                    xmlClose("notes")
                    continue
                }

                if (!data.classList.contains("attachment")) {

                    // const data = el.firstElementChild   // always div[class="category_data subfield ???"]
                    if (["text", "grouped_subfield"].some(id => data.classList.contains(id))) {
                        xmlOpen("subfield")
                        for (const it of data.childNodes) {
                            // console.log("??", it.nodeName, it.textContent)
                            if (it.nodeName === "SPAN") {
                                const name = camelizeId(it.classList.item(0))
                                // xmlLog(name, it.innerHTML)
                                parseSubField(name, it.innerHTML)
                            } else if (it.nodeName === "#text" || it.nodeName === "P") {
                                // if (it.nodeName === "P" && it.childNodes.length > 1) console.log(it.childNodes[0].textContent)

                                if (it.nodeName === "P" && it.childNodes && it.childNodes.length > 1 && it.childNodes[0].nodeName === "STRONG" && it.childNodes[0].textContent.trim() === "note:")
                                    xmlLog("subfieldNote", it.childNodes[1].textContent)
                                else if (it.nodeName === "P")
                                    xmlLog("paragraph", it.textContent)
                                else
                                    xmlLog("text", it.textContent)
                            } else if (it.nodeName === "STRONG") {
                                xmlLog("strong", it.textContent)
                            } else if (it.nodeName === "BR") {
                                // Ignore
                            } else console.error(`ERROR> - not processing: ${it.nodeName}, ${it.textContent}`)
                        }
                        xmlClose("subfield")
                    } else if (data.classList.contains("numeric") || data.classList.contains("historic")) {
                        xmlOpen("subfield")
                        for (const subfield of data.children) {
                            const className = subfield.classList[0];    // example: subfield-number
                            const name = camelizeId(className)
                            parseSubField(name, subfield.innerHTML)
                        }
                        xmlClose("subfield")

                    } else if (data.classList.length === 0) {
                        xmlOpen("reference")
                        for (const subfield of data.children) {
                            const className = subfield.classList[0];    // example: subfield-number
                            const name = camelizeId(className)

                            if (name === "categoryData") {
                                // parseSubField("link", subfield.children[0].innerHTML)
                                const a = subfield.children[0]
                                xmlLog("link", a.innerHTML, `url="${baseUrl + a.getAttribute('href')}"`)
                            }
                            // else
                            //     parseSubField(name, subfield.innerHTML)
                        }
                        xmlClose("reference")
                    } else console.error("ERROR>", data.nodeName, JSON.stringify(data.classList), data.textContent)
                    // TODO ? attachment, 

                }
            }
            xmlClose("field")
        }
        xmlClose("section")
    }
    xmlClose("country")
}

/**
 * Function definitions
 */


function getValuesStr(arr) {
    let text = ""
    for (const e of arr)
        text += e.innerHTML.trim()

    return text
}

function camelizeId(str) {
    return str.replace(/(?:^\w|[A-Z]|[-_][A-Za-z0-9])/g, function (word, index) {
        return index === 0 ? word.toLowerCase() : word.slice(1).toUpperCase();
    }).replace(/\s+/g, '');
}

function xmlOpen(elName, attributes) {
    log(`<${elName}${attributes ? " " + attributes : ""}>`)
}
function xmlClose(elName) {
    xmlOpen("/" + elName)
}

function xmlLog(elName, text, attributes) {
    if (text.trim().length === 0) return
    xmlOpen(elName, attributes)
    log(escapeHtml(text))
    xmlClose(elName)

    return 1
}

function parseSubField(name, text) {
    text = text.trim()
    if (text.length === 0) return

    if (name === "subfieldNumber")
        return parseSubfieldNumber(text)

    if (name === "subfieldDate")
        return parseSubfieldDate(text)
    // if (name !== "subfieldName") console.error("not spec>", name)
    xmlLog(name, text)
}

function parseSubfieldNumber(text) {
    const reg = /([$])?([+-]?[0-9\.,]+)(\ )?(.*)?/ // matches: [number, space, unit]
    // TODO $2323 bilion -> currency?
    const [_, dolar, number, space, unit] = text.match(reg)

    let attributes = unit ? `unit="${unit}"` : undefined
    if (dolar) {
        const currency = `currency="USD"`
        attributes = attributes ? attributes + " " + currency : currency;
    }

    return xmlLog("subfieldNumber", number.replace(/,/g, ""), attributes ? attributes : undefined)
}

function parseSubfieldDate(text) {
    /*
        (September 2019)
        (2019)
        (2019 est.)
 
    */
    if (/^\([0-9]{4}/.test(text))
        return xmlLog("subfieldYear", text.match(/[0-9]{4}/)[0], text.endsWith(" est.)") ? 'estimate="true"' : undefined)

    function getMonth(text) {
        const month = text.split(" ")[0].slice(1, -1)
        return new Date(Date.parse(month + " 1, 2012")).getMonth() + 1
    }
    //2002-09-24
    if (/^\([A-Z][a-z]+\ [0-9]{4}\)$/.test(text))
        return xmlLog("subfieldDate", text.match(/[0-9]{4}/) + "-" + String(getMonth(text)).padStart(2, "0") + "-01")
}

function containsSlashText(node) {
    return node.textContent.trim() === "/"
}

function escapeHtml(unsafe) {
    return unsafe
        .replace(/&/g, "&amp;")
        .replace(/</g, "&lt;")
        .replace(/>/g, "&gt;")
        .replace(/"/g, "&quot;")
        .replace(/'/g, "&#039;");
}