function main() {
    const sectionH2 = document.querySelectorAll(".section > h2")
    sectionH2.forEach(h2 => h2.addEventListener("click", function () {
        const classes = this.nextElementSibling.classList

        if (classes.contains("show")) {
            classes.remove("show")
        } else classes.add("show")
        console.log("click event")
    }))

    if (window.location.hash !== "" && window.location.hash !== "#") {
        const selectedSection = window.location.hash
        const sectionHeader = document.querySelector(selectedSection)
        sectionHeader.nextElementSibling.classList.add("show")

        const y = sectionHeader.getBoundingClientRect().top + window.scrollY;
        window.scroll({
            top: y,
            behavior: 'smooth'
        });
    }
}

window.addEventListener('load', (event) => {
    main()
    console.log("loaded")
});