<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version="2.0">

    <xsl:output method="html" omit-xml-declaration="yes" indent="yes"/>

    <xsl:template match="/">
        <html>
            <head>
                <title>Countries</title>
                <link rel="stylesheet" href="media/css/styles.css" />
                <link rel="stylesheet" href="media/css/panda.css" />
            </head>
            <body>
                <div class="index">
                    <div class="panda">
                        <div class="ears">
                            <div class="leftEar" />
                            <div class="rightEar" />
                        </div>
                        <div class="head">
                            <div class="eyes">
                                <div class="leftEye">
                                    <div class="irisEye" />
                                </div>
                                <div class="rightEye">
                                    <div class="irisEye" />
                                </div>
                            </div>
                            <div class="face">
                                <div class="nose">
                                    <div class="lineDown" />
                                </div>
                                <div class="mouth">
                                    <div class="line" />
                                </div>
                            </div>
                        </div>
                        <!-- <div class="hands" /> -->
                    </div>
                    <h1>Content:</h1>
                    <xsl:for-each select="countries/country">
                        <xsl:variable name="countryFile" select="concat(@code, '.html')"/>
                        <ul>
                            <li>
                                <a href="{$countryFile}">
                                    <xsl:value-of select="@title"/>
                                </a>
                                <ul>
                                    <xsl:for-each select="section">
                                        <li>
                                            <a href="{$countryFile}#{@class}">
                                                <xsl:value-of select="@title" />
                                            </a>
                                        </li>
                                    </xsl:for-each>
                                </ul>
                            </li>
                        </ul>
                    </xsl:for-each>
                    <xsl:apply-templates select="countries/country" />
                    <xsl:call-template name="calculate"/>
                </div>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="country">
        <xsl:result-document href="output/{@code}.html">
            <html>
                <head>
                    <title>
                        <xsl:value-of select="@title"/>
                    </title>
                    <link rel="stylesheet" href="media/css/styles.css" />
                    <script type="text/javascript" src="media/js/main.js"></script>
                </head>
                <body>
                    <a href="">
                        <h1 class="header">
                            <xsl:value-of select="@title"/>
                        </h1>
                    </a>
                    <div class="body" id="{info/travelFacts/local-name()}">
                        <xsl:apply-templates select="section" />
                    </div>
                </body>
            </html>
        </xsl:result-document>
    </xsl:template>

    <xsl:template match="section">
        <div class="{local-name()}">
            <h2 id="{@class}">
                <xsl:value-of select="@title"/>
            </h2>
            <div class="section-content">
                <xsl:apply-templates/>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="field">
        <div class="{local-name()}">
            <h3>
                <xsl:value-of select="@title"/>
            </h3>

            <xsl:variable name="className">
                <xsl:if test="subfield/subfieldGroup">
                    <xsl:text>group</xsl:text>
                </xsl:if>
            </xsl:variable>

            <div class="field-content {$className}">
                <xsl:apply-templates/>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="subfield">
        <div class="{local-name()}">
            <xsl:apply-templates/>
        </div>
    </xsl:template>

    <xsl:template match="reference">
        <div class="{local-name()}">
            <span>
                country comparison to the world:
            </span>
            <xsl:apply-templates/>
        </div>
    </xsl:template>

    <xsl:template match="groupedSubfield">
        <div class="{local-name()}">
            <xsl:for-each select="subfield">
                <xsl:apply-templates/>
                <xsl:if test="position() != last()">
                    <xsl:text> / </xsl:text>
                </xsl:if>
            </xsl:for-each>
        </div>
    </xsl:template>

    <xsl:template match="subfieldName|subfieldGroup|strong">
        <span class="{local-name()}">
            <xsl:value-of select="concat(normalize-space(.), ':')"/>
        </span>
    </xsl:template>

    <xsl:template match="subfieldNumber">
        <xsl:variable name="number">
            <xsl:value-of select="concat(@currency, ' ')"/>
            <xsl:choose>
                <xsl:when test="contains(., '.')">
                    <xsl:value-of select="format-number(., '#,##0.00')"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="format-number(., '#,###')"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <span class="{local-name()}">
            <xsl:value-of select="normalize-space(concat($number, ' ', @unit))"/>
        </span>
    </xsl:template>

    <xsl:template match="subfieldNote">
        <span class="{local-name()}">
            <xsl:value-of select="normalize-space(.)"/>
        </span>
    </xsl:template>

    <xsl:template match="subfieldDate|subfieldYear">
        <span class="{local-name()}">
            <xsl:variable name="date" select="normalize-space(.)"/>
            <xsl:text>(</xsl:text>
            <xsl:choose>
                <xsl:when test="name() = 'subfieldYear'">
                    <xsl:value-of select="$date"/>
                </xsl:when>
                <xsl:otherwise>
                    <!-- 2019-09-01 -->
                    <xsl:variable name="month" select="number(substring($date, 6, 2))"/>
                    <xsl:choose>
                        <xsl:when test="$month=1">January </xsl:when>
                        <xsl:when test="$month=2">February </xsl:when>
                        <xsl:when test="$month=3">March </xsl:when>
                        <xsl:when test="$month=4">April </xsl:when>
                        <xsl:when test="$month=5">May </xsl:when>
                        <xsl:when test="$month=6">June </xsl:when>
                        <xsl:when test="$month=7">July </xsl:when>
                        <xsl:when test="$month=8">August </xsl:when>
                        <xsl:when test="$month=9">September </xsl:when>
                        <xsl:when test="$month=10">October </xsl:when>
                        <xsl:when test="$month=11">November </xsl:when>
                        <xsl:when test="$month=12">December </xsl:when>
                    </xsl:choose>
                    <xsl:value-of select="substring($date, 1, 4)"/>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:if test="@estimate">
                <xsl:text> est.</xsl:text>
            </xsl:if>
            <xsl:text>)</xsl:text>
        </span>
    </xsl:template>

    <xsl:template match="note">
        <div>

            <xsl:apply-templates select="strong"/>
            <xsl:value-of select="text"/>

        </div>
    </xsl:template>

    <xsl:template match="paragraph">
        <p>
            <xsl:value-of select="."/>
        </p>
    </xsl:template>
    <xsl:template match="text">
        <xsl:value-of select="normalize-space(.)"/>
        <xsl:if test="following-sibling::* and not(following-sibling::*[1]/local-name() = ('subfieldYear', 'subfieldDate'))">
            <br/>
        </xsl:if>
    </xsl:template>

    <xsl:template match="link">
        <a href="{@url}">
            <xsl:value-of select="." />
        </a>
    </xsl:template>

    <!-- 
        XSLT rule for text nodes says "copy them to the output":
        <xml>
        <data> value </data>
        </xml>

        contains three text nodes:

        1. "\n··" (right after <xml>)
        2. "·value·"
        3. "\n" (right before </xml>)
        So mute all by default -> new lines in formated xml will be ignored
    -->
    <xsl:template match="*/text()">
        <xsl:value-of select="normalize-space()"/>
    </xsl:template>

    <xsl:template name="calculate">
        <xsl:variable name="sections" select="//countries/country/section"/>
        <xsl:variable name="population_total" select="$sections/field[@title = 'Population']/subfield/subfieldNumber" />
        <xsl:variable name="populationAvg" select="sum($population_total) div count($population_total)"/>
        <xsl:variable name="boundaries_total" select="$sections/field[@title = 'Land boundaries']/subfield/subfieldName[normalize-space(text()) = 'total']/../subfieldNumber" />
        <xsl:variable name="landBounderies" select="sum($boundaries_total) div count($boundaries_total)"/>
        <xsl:variable name="totalArea" select="$sections[@title = 'Geography']/field[@title = 'Area']/subfield/subfieldName[normalize-space(text()) = 'total']/../subfieldNumber"/>
        <xsl:variable name="maxArea" select="max($totalArea)"/>
        <xsl:variable name="countryMaxArea" select="countries/country[./section[@title = 'Geography']/field[@title = 'Area']/subfield/subfieldNumber = $maxArea]/@title"/>
        <table>
            <tr>
                <th>Veličina</th>
                <th>Hodnota</th>
            </tr>
            <tr>
                <td>Průměrná populace</td>
                <td>
                    <xsl:value-of select="format-number($populationAvg, '#,###')"/>
                </td>
            </tr>
            <tr>
                <td>Průměrná délka hranic</td>
                <td>
                    <xsl:value-of select="format-number($landBounderies, '#,###')"/>
                </td>
            </tr>
            <tr>
                <td>Největší země</td>
                <td>
                    <xsl:value-of select="concat($countryMaxArea, ' - ', format-number($maxArea, '#,###'), ' ', $totalArea[1]/@unit)"/>
                </td>
            </tr>
        </table>

    </xsl:template>
</xsl:stylesheet>
