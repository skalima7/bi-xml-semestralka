<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <xsl:output method="xml" indent="yes"/>

    <xsl:template match="/">
        <fo:root>
            <fo:layout-master-set>
                <fo:simple-page-master master-name="index" page-height="29.7cm" page-width="21.0cm" margin="2cm">
                    <fo:region-body margin-bottom="10mm" margin="25mm" margin-top="37mm"/>
                    <fo:region-after extent="8mm"/>
                </fo:simple-page-master>
                <fo:simple-page-master master-name="country" page-height="29.7cm" page-width="21.0cm" margin="2cm">
                    <fo:region-body margin-bottom="10mm"/>
                    <fo:region-after extent="8mm"/>
                </fo:simple-page-master>
                <fo:page-sequence-master master-name="my-sequence-index">
                    <fo:repeatable-page-master-reference master-reference="index"/>
                </fo:page-sequence-master>
                <fo:page-sequence-master master-name="my-sequence-master">
                    <fo:repeatable-page-master-reference master-reference="country" maximum-repeats="1000"/>
                </fo:page-sequence-master>
            </fo:layout-master-set>
            <fo:page-sequence master-reference="my-sequence-index">
                <!-- Společný obsah všech stránek v zápatí stránky -->
                <fo:static-content flow-name="xsl-region-after">
                    <fo:block>
                        <xsl:text>Strana </xsl:text>
                        <fo:page-number/>
                        <xsl:text> z </xsl:text>
                        <fo:page-number-citation ref-id="last_page"/>
                    </fo:block>
                </fo:static-content>

                <fo:flow flow-name="xsl-region-body" font-family="Arial,Helvetica,sans-serif" font-size="11pt">
                    <fo:block>
                        <fo:block font-size="16pt" font-weight="bold" text-align="center">TABLE OF CONTENTS</fo:block>


                        <!-- 
                            country name
                                |-> One page summary
                                    |-> list item per section
                                |->travel facts
                            country name
                                |-> One page summary
                                    |-> list per section
                                |->travel facts
                            ...

                            It is complicated, because it have nested lists
                         -->
                        <xsl:for-each select="countries/country">
                            <xsl:variable name="countryFile" select="@code"/>
                            <fo:list-block provisional-distance-between-starts="1cm" provisional-label-separation="1cm">
                                <fo:list-item>
                                    <fo:list-item-label>
                                        <fo:block>

                                        </fo:block>
                                    </fo:list-item-label>
                                    <fo:list-item-body>
                                        <fo:block text-align-last="justify">
                                            <fo:basic-link internal-destination="{$countryFile}" font-weight="bold">
                                                <xsl:value-of select="@title"/>
                                            </fo:basic-link>
                                            <fo:leader leader-pattern="dots" />
                                            <fo:basic-link internal-destination="{$countryFile}">
                                                <fo:page-number-citation ref-id="{$countryFile}"/>
                                            </fo:basic-link>
                                        </fo:block>
                                    </fo:list-item-body>
                                </fo:list-item>
                                <fo:list-item>
                                    <fo:list-item-label>
                                        <fo:block>
                                        </fo:block>
                                    </fo:list-item-label>
                                    <fo:list-item-body>

                                        <fo:list-block margin-left="1cm">
                                            <xsl:for-each select="section">
                                                <fo:list-item>
                                                    <fo:list-item-label>
                                                        <fo:block text-align-last="justify">
                                                            <fo:basic-link internal-destination="{$countryFile}#{@class}">
                                                                <xsl:value-of select="@title"/>
                                                            </fo:basic-link>
                                                            <fo:leader leader-pattern="dots" />
                                                            <fo:basic-link internal-destination="{$countryFile}#{@class}">
                                                                <fo:page-number-citation ref-id="{$countryFile}#{@class}"/>
                                                            </fo:basic-link>
                                                        </fo:block>
                                                    </fo:list-item-label>
                                                    <fo:list-item-body>
                                                        <fo:block />
                                                    </fo:list-item-body>
                                                </fo:list-item>
                                            </xsl:for-each>
                                        </fo:list-block>
                                    </fo:list-item-body>
                                </fo:list-item>
                            </fo:list-block>
                        </xsl:for-each>
                    </fo:block>
                </fo:flow>
            </fo:page-sequence>
            <fo:page-sequence master-reference="my-sequence-master">
                <fo:static-content flow-name="xsl-region-after">
                    <fo:block>
                        <xsl:text>Strana </xsl:text>
                        <fo:page-number/>
                        <xsl:text> z </xsl:text>
                        <fo:page-number-citation ref-id="last_page"/>
                    </fo:block>
                </fo:static-content>
                <fo:flow flow-name="xsl-region-body" font-family="Arial,Helvetica,sans-serif" font-size="11pt">
                    <xsl:apply-templates select="countries/country" />
                    <fo:block id="last_page"/>
                </fo:flow>
            </fo:page-sequence>

        </fo:root>
    </xsl:template>

    <xsl:template match="country">
        <fo:block page-break-before="always" id="{@code}">
            <!-- Right watermark -->
            <fo:block-container position="absolute" height="19.5cm" width="1.9cm" right="-2.1cm" top="-1.3cm" background-image="url('media/images/watermark.png')" background-position="left" background-color="transparent">
                <fo:block/>
            </fo:block-container>
            <fo:block font-size="22pt" font-weight="bold" text-align="center">
                <xsl:value-of select="@title"/>
            </fo:block>

            <xsl:apply-templates select="section">
                <xsl:with-param name="code" select="@code"/>
            </xsl:apply-templates>
        </fo:block>
    </xsl:template>

    <xsl:template match="section">
        <xsl:param name="code" />
        <fo:block>
            <xsl:call-template name="h2">
                <xsl:with-param name="text" select="@title"/>
                <xsl:with-param name="id" select="concat($code, '#', @class)"/>
            </xsl:call-template>

            <fo:block>
                <xsl:apply-templates/>
            </fo:block>
        </fo:block>
    </xsl:template>

    <xsl:template match="field">
        <fo:block keep-with-next.within-page="always">
            <xsl:call-template name="h3">
                <xsl:with-param name="text" select="@title"/>
            </xsl:call-template>
            <fo:block>
                <xsl:apply-templates/>
            </fo:block>
        </fo:block>
    </xsl:template>


    <xsl:variable name="rightPadding">3px</xsl:variable>
    <xsl:variable name="bottomPadding">5px</xsl:variable>

    <xsl:template match="subfield">
        <fo:block padding-bottom="{$bottomPadding}">
            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>

    <xsl:template match="groupedSubfield">
        <fo:block>
            <xsl:for-each select="subfield">
                <xsl:apply-templates/>
                <xsl:if test="position() != last()">
                    <xsl:text> / </xsl:text>
                </xsl:if>
            </xsl:for-each>
        </fo:block>
    </xsl:template>

    <xsl:template match="subfieldName|subfieldGroup|strong">
        <fo:inline font-weight="bold" padding-right="{$rightPadding}">
            <xsl:value-of select="concat(normalize-space(.), ':')"/>
        </fo:inline>
    </xsl:template>

    <xsl:template match="reference">
        <fo:block padding-bottom="{$bottomPadding}">
            <fo:inline font-weight="bold" padding-right="{$rightPadding}">
              country comparison to the world:
            </fo:inline>
            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>

    <xsl:template match="subfieldNumber">
        <xsl:variable name="number">
            <xsl:value-of select="concat(@currency, ' ')"/>
            <xsl:choose>
                <xsl:when test="contains(., '.')">
                    <xsl:value-of select="format-number(., '#,##0.00')"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="format-number(., '#,###')"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <fo:inline padding-right="{$rightPadding}">
            <xsl:value-of select="normalize-space(concat($number, ' ', @unit))"/>
        </fo:inline>
    </xsl:template>

    <xsl:template match="subfieldNote">
        <fo:inline padding-right="{$rightPadding}">
            <xsl:value-of select="normalize-space(.)"/>
        </fo:inline>
    </xsl:template>

    <xsl:template match="subfieldDate|subfieldYear">
        <fo:inline>
            <xsl:variable name="date" select="normalize-space(.)"/>
            <xsl:text>(</xsl:text>
            <xsl:choose>
                <xsl:when test="name() = 'subfieldYear'">
                    <xsl:value-of select="$date"/>
                </xsl:when>
                <xsl:otherwise>
                    <!-- 2019-09-01 -->
                    <xsl:variable name="month" select="number(substring($date, 6, 2))"/>
                    <xsl:choose>
                        <xsl:when test="$month=1">January </xsl:when>
                        <xsl:when test="$month=2">February </xsl:when>
                        <xsl:when test="$month=3">March </xsl:when>
                        <xsl:when test="$month=4">April </xsl:when>
                        <xsl:when test="$month=5">May </xsl:when>
                        <xsl:when test="$month=6">June </xsl:when>
                        <xsl:when test="$month=7">July </xsl:when>
                        <xsl:when test="$month=8">August </xsl:when>
                        <xsl:when test="$month=9">September </xsl:when>
                        <xsl:when test="$month=10">October </xsl:when>
                        <xsl:when test="$month=11">November </xsl:when>
                        <xsl:when test="$month=12">December </xsl:when>
                    </xsl:choose>
                    <xsl:value-of select="substring($date, 1, 4)"/>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:if test="@estimate">
                <xsl:text> est.</xsl:text>
            </xsl:if>
            <xsl:text>)</xsl:text>
        </fo:inline>
    </xsl:template>

    <xsl:template match="note">
        <fo:block>
            <xsl:apply-templates />
        </fo:block>
    </xsl:template>

    <xsl:template match="link">
        <fo:inline>
            <!-- Hash in URL is beining replaced by "%", removing it -->
            <fo:basic-link external-destination="url({substring(@url, 0, string-length(@url) - 2)})" text-decoration="underline">
                <xsl:value-of select="."/>
            </fo:basic-link>
        </fo:inline>
    </xsl:template>

    <xsl:template match="paragraph">
        <fo:block padding-top="3px" padding-bottom="3px">
            <xsl:value-of select="."/>
        </fo:block>
    </xsl:template>

    <xsl:template match="text">
        <fo:inline padding-right="{$rightPadding}">
            <xsl:value-of select="normalize-space(.)"/>
        </fo:inline>
        <xsl:if test="following-sibling::* and not(following-sibling::*[1]/local-name() = ('subfieldYear', 'subfieldDate'))">
            <xsl:call-template name="br" />
        </xsl:if>
    </xsl:template>

    <!-- 
        XSLT rule for text nodes says "copy them to the output":
        <xml>
        <data> value </data>
        </xml>

        contains three text nodes:

        1. "\n··" (right after <xml>)
        2. "·value·"
        3. "\n" (right before </xml>)
        So mute all by default -> new lines in formated xml will be ignored
    -->
    <xsl:template match="*/text()">
        <xsl:value-of select="normalize-space()"/>
    </xsl:template>

    <xsl:template name="h2">
        <xsl:param name="text" />
        <xsl:param name="id" />
        <fo:block font-size="18pt" id="{$id}" padding-top="10px" font-weight="700">
            <xsl:value-of select="$text" />
        </fo:block>
    </xsl:template>

    <xsl:template name="h3">
        <xsl:param name="text" />
        <fo:block font-size="14pt" margin-top="5px" color="#337ab7">
            <xsl:value-of select="$text" />
        </fo:block>
    </xsl:template>

    <xsl:template name="br">
        <fo:block>
        </fo:block>
    </xsl:template>
</xsl:stylesheet>
