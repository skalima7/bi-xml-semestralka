#!/bin/bash

rm -rf output/epub
cp -r epub output/
mkdir -p output/epub/OEBPS
cp -r output/media output/epub/OEBPS
rm output/epub/OEBPS/media/images/watermark.png # no need, only for pdf

echo "Preparing xhtml files"
saxon all.xml xml-xhtml.xsl >output/index.xhtml

cd output
for filename in *.xhtml; do
    # Add missing default namespace to html element
    sed 's/<html>/<html xmlns="http:\/\/www.w3.org\/1999\/xhtml" xml:lang="en">/' \
        $filename >epub/OEBPS/$filename
done

cd ..

echo "Preparing epub files"
saxon all.xml epub-opf.xsl >output/epub/OEBPS/book.opf
saxon all.xml epub-ncx.xsl >output/epub/OEBPS/book.ncx

cd output/epub
zip -X MyNewEbook.epub mimetype
zip -rg MyNewEbook.epub META-INF -x \*.DS_Store
zip -rg MyNewEbook.epub OEBPS -x \*.DS_Store
mv MyNewEbook.epub ../
cd ../..

echo "Generation DONE."
echo "Cleaning..."
rm -rf output/epub
rm -rf output/*.xhtml

echo ""
echo "Running ePub validations..."
java -jar ./epubcheck-4.2.2/epubcheck.jar output/MyNewEbook.epub
