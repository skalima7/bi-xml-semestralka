<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version= "2.0" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="xml" encoding="utf-8" indent="yes" />

    <xsl:template match="/">
        <!--
<!DOCTYPE ncx PUBLIC "-//NISO//DTD ncx 2005-1//EN" "http://www.daisy.org/z3986/2005/ncx-2005-1.dtd">
-->
        <ncx xmlns="http://www.daisy.org/z3986/2005/ncx/" version="2005-1">

            <head>
                <meta name="dtb:uid" content="BookId1234" />
                <meta name="dtb:depth" content="1" />
                <meta name="dtb:totalPgeCount">
                    <xsl:attribute name="content">
                        <xsl:value-of select='count(countries/country)' />
                    </xsl:attribute>
                </meta>
                <meta name="dtb:maxPageNumber">
                    <xsl:attribute name="content">
                        <xsl:value-of select='count(countries/country)' />
                    </xsl:attribute>
                </meta>
            </head>

            <docTitle>
                <text>Semestrálka XML</text>
            </docTitle>

            <!-- <docAuthor>
                <text>
                    Martin Skalický
                </text>
            </docAuthor> -->

            <navMap>
                <navPoint id="index" playOrder="1">
                    <navLabel>
                        <text>Index</text>
                    </navLabel>
                    <content src="index.xhtml"/>
                </navPoint>

                <xsl:for-each select="countries/country">
                    <xsl:variable name="countryFile">
                        <xsl:value-of select="concat('country', position(), '.xhtml')"/>
                    </xsl:variable >
                    <xsl:variable name="countryID">
                        <xsl:value-of select="@id"/>
                    </xsl:variable >
                    <xsl:variable name="countryName">
                        <xsl:value-of select="name"/>
                    </xsl:variable >

                    <navPoint id="{$countryID}" playOrder="{position() + 1}">
                        <navLabel>
                            <text>
                                <xsl:value-of select='$countryName'/>
                            </text>
                        </navLabel>
                        <content src="{$countryFile}"/>
                    </navPoint>

                    <xsl:for-each select="info/onePageSummary/child::*">
                        <navPoint id="{concat($countryID, '_', local-name())}">
                            <navLabel>
                                <text>
                                    <xsl:value-of select="concat($countryName, ' - ', local-name())"/>
                                </text>
                            </navLabel>
                            <content src="{concat($countryFile, '#', local-name())}"/>
                        </navPoint>
                    </xsl:for-each>
                </xsl:for-each>

            </navMap>
        </ncx>
    </xsl:template>

</xsl:stylesheet>