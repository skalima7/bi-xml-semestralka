<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version= "2.0" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="xml" encoding="utf-8" indent="yes" />

    <xsl:template match="/">
        <package version="2.0" 
            xmlns="http://www.idpf.org/2007/opf" unique-identifier="BookId1234">

            <metadata xmlns:dc="http://purl.org/dc/elements/1.1/" 
                xmlns:opf="http://www.idpf.org/2007/opf">

                <dc:title>Semestrálka XML</dc:title>
                <dc:creator opf:role="aut">Martin Skalický</dc:creator>
                <dc:language>en-US</dc:language>
                <dc:identifier id="BookId1234">BookId1234</dc:identifier>

            </metadata>

            <manifest>
                <item id="index" href="index.xhtml" media-type="application/xhtml+xml" />
                <xsl:for-each select='//countries/country'>
                    <item id="{name}" href="country{position()}.xhtml" media-type="application/xhtml+xml" />
                    <xsl:for-each select='images/image'>
                        <item media-type="image/png" id="{translate(@alt, ' ', '_')}">
                            <xsl:attribute name="href">
                                <xsl:value-of select="@src"/>
                            </xsl:attribute>
                        </item>
                    </xsl:for-each>
                </xsl:for-each>
                <item id="ncx" href="book.ncx" media-type="application/x-dtbncx+xml" />
                <item id="panda" href="media/css/panda.css" media-type="text/css" />
                <item id="styles" href="media/css/styles.css" media-type="text/css" />
                <!-- <item media-type="image/jpeg" id="cover" href="Images/cover.jpg"/> -->
                <!-- again, LilyPond related images -->


                <!-- add fonts manually -->

                <!--
	  	<item media-type="application/x-font-otf" id="" href="Fonts/xyz.otf" />
	  	<item media-type="application/x-font-ttf" id="" href="Fonts/xyz.ttf" />
	  	-->
            </manifest>

            <spine toc="ncx">
                <itemref idref="index" />
                <xsl:for-each select='//countries/country'>
                    <itemref idref="{name}" />
                </xsl:for-each>

                <!-- <xsl:for-each select='//section'>
                    <itemref>
                        <xsl:attribute name="idref">aut_<xsl:value-of select="@implicit"/>
-html</xsl:attribute>
                    </itemref>
                </xsl:for-each> -->
            </spine>

        </package>
    </xsl:template>

</xsl:stylesheet>