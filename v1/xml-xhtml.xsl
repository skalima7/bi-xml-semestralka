<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' 
    xmlns="http://www.w3.org/1999/xhtml" version="2.0">
    <xsl:output method="xhtml" omit-xml-declaration="no" indent="yes" media-type="text/html" encoding="utf-8" doctype-public="-//W3C//DTD XHTML 1.1//EN" doctype-system="http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"/>
    <xsl:variable name="ext" select="'.xhtml'"/>
    <xsl:variable name="mode">xhtml</xsl:variable>
    <xsl:include href="html-templates.xsl" />
</xsl:stylesheet>