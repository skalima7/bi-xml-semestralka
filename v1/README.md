# bi-xml-semestralka
Projekt je zaměřen na vyzkoušení možností xml. Ze stránky https://www.cia.gov/library/publications/the-world-factbook/ v sekcich "ONE-PAGE COUNTRY SUMMARIES" a "TRAVEL FACTS" jsem si vybral 4 země a informace o nich jsem si převedl do xml (`namibia.xml, netherlands.xml, nicaragua.xml, norway.xml`) - převážně ručně a část jsem si naskriptoval v JS. Nejprve jsem si navrhl xml strukturu, ta se nachází v `xml-template.xml`. Následně jsem vytvořil validační struktury DTD (`xml-template.dtd`) a pokročilejší RELAX NG s kompaktní syntaxí (`xml-template.rnc`). Pro výstup jsem zvolit PDF a HTML formát - využil jsem možností XSL (XSL-FO a XSLT). Transformace do html se nachází v `xml-html.xsl` a css s formátováním v `/output/media/css/styles.css`. Transformaci do PDF obsahuje `xml-pdf.xsl`. Jak PDF, tak i HTML výstupy obsahují indexovou stránku s odkazy na jednotlivé země a jejich sekce (government, economy...). Samotné stránky s podrobnými informacemi o dané zemi jsou formátovány a obsahují obrázky, které jsou umístěny `/output/media/images/`. 

Dále jsem vytvořil výstup do EPUB formátu pro čtečky elektronických knih. Transformace se nachází `epub-ncx.xsl` a `epub-opf.xsl`. Pro generování xhtml jsem využil stejné XSLT template jako pro html - obě mají společné `html-templates.xsl`, a specifická transformace je v `xml-html.xsl` pro html a v `xml-xhtml.xsl` pro xhtml výstup. Bash skript pro vytvoření EPUB souboru - `epub-create.sh`.

### Výstup
Výstup s PDF, HTML a EPUB se nachází: `/output/{index.html, out.pdf, MyNewEbook.epub}`

### Použitý SW
* jing-trang
* saxon
* fop
* epubcheck

## XML
U návrhu struktur XML jsem vycházel ze struktury PDF dokumentu, ze kterého jsem čerpal dané informace. Pro každou zemi jsou data strukturovány do dvou hlavních sekcí - onePageSummary a travelFacts. Každá země má UID, které využívám pro referencování ve výstupních dokumentech PDF a HTML. Zjednodušené schéma:
```xml
<country id="C_name">
    <name></name>
    <info>
        <onePageSummary>
            <government>
               ...
            </government>
            <geography>
              ...
            </geography>
            <economy>
               ...
            </economy>
            <society>
              ...
        </onePageSummary>
        <travelFacts>
            <fact>
                <name></name>
                <text></text>
            </fact>
            ...
            <footer>
                <name></name>
                <link url=""></link>
                <updated>YYY-MM-DD</updated>
            </footer>
        </travelFacts>
    </info>
    <images>
        <image src="../images/" alt=""/>
    </images>
</country>
```

## DTD
Jednotlivé země jsou spojené pomocí DTD do jednoho xml dokumentu v `all-using-dtd.xml`. Nad tímto dokumentem lze spustit DTD validaci:
```bash
xmllint --valid  --noout all-using-dtd.xml  # using DTD
```
nebo získat spojený dokument `all.xml` následujícím příkazem:
```bash
xmllint --noent all-using-dtd.xml | grep -E -v '<!DOCTYPE|]>|<!ENTITY' > all.xml
```

## Relax NG
Validaci pomocí kompaktní Relax NG syntaxe lze spustit příkazem:
```bash
jing -c xml-template.rnc all.xml  
```
nebo nejprve přeložit kompaktní syntaxi do xml a následně zvalidovat:
```bash
trang -I rnc -O rng xml-template.rnc output/validation.rng 
jing 'output/validation.rng' 'all.xml'
```
Z pokročilejších validací jsem využil: datové typy, omezení max/min číselných hodnot, znovu použití elementů, kontrolu patternu.

## XSL-FO
První strana PDF je index všech zemí a má jiné rozložená než ostatní. Index obsahuje odkazy na dané země a jejich sekce na které lze kliknout (odkaz). V zápatí je vždy uvedeno číslo stránky. Informacemi o zemi jsou rozděleny do odstavců a vždy jsou dva vedle sebe. Stránka s TravelFacts obsahuje tabulku a v pravé části obsahuje znak "World factbook" vložený jako background-image. Generování:
```bash
saxon -s:'all.xml' -xsl:xml-pdf.xsl -o:output/f.fo  && fop -fo 'output/f.fo' -pdf 'output/out.pdf'
```

## XSLT
`/output/index.html` je rozcestník, obsahující odkazy na jednotlivé země a jejich sekce. Každá země má vygenerovanou vlastní html stránku na kterou odkaz směřuje. Jednotlivé sekce jsou generovány jako odstavce, kde první tři jsou vedle sebe a další zabírá celou šíři stránky. Pro TravelFacts je generována tabulka a pod ní se nachází zápatí. Generování.
```bash
saxon all.xml xml-html.xsl > output/index.html
```

## EPUB
Primárně jsem si chtěl vyzkoušet způsob generování epub souboru. Nakonec jsou pomocí xsl generovány jak veškeré xhtml soubory, tak i přidružená metadata (soubory `.opf` a `.ncx`). Generování pomocí připraveného skriptu:
```bash
sh epub-create.sh
```
Chtěl jsem využít stejné šablony jak pro xhtml tak i pro html. Využil jsem elementy, které jsou podporovány oběma jazyky (Z html5 jsem původně stejně využíval navíc pouze `<footer>` a `<label>`), tyto šablony jsou v souboru `html-templates.xsl`. Potom již tuto šablonu pomocí xsl:include načtu do specifického xsl souboru pro daný výstup - html/xhtml + pomocí globální proměnné modifikuji jakou příponu mají mít vygenerované soubory a odkazy .xhtml/.html. HTML výstup takto funguje bez chyb, problém mi nastal při generování xhtml, to potřebuje mít nastavený globální namespace, aby se správně aplikoval attribut 'xmlns' do html elementu a byl to tak validní xhtml soubor. XSL bohužel neumí modifikovat za běhu globální namespace, jedinné řešení je využít dvojitou konverzi - nejprve si vygenerovat soubor bez namespace a druhou transformací tam namespace dodat. Ale protože se jedná o úpravu jednoho atributu u elementu html, tak jako nejjednodušší řešení jsem zvolil jednoduchý script v bash, který zavolá transformaci, upraví 1 řádek, tak aby soubor byl validní xhtml a rovnou vše zabalí do výsledného epub. Stejná transformace by šla udělat i v XSL, ale když jsem stejně psal bash pro celý proces vytvoření epub, tak mi to přijde elegantnější takto.


# TL;RD COMMANDS
## Merge xml files into one
```bash
xmllint --noent all-using-dtd.xml | grep -E -v '<!DOCTYPE|]>|<!ENTITY' > all.xml
```

## translate rnc -> rng
```bash
trang -I rnc -O rng xml-template.rnc output/validation.rng 
```

## Validace
```bash
xmllint --valid  --noout all-using-dtd.xml  # using DTD
jing -c xml-template.rnc all.xml            # using RELAX NG rnc
jing 'output/validation.rng' 'all.xml'      # using rng

java -jar ./epubcheck-4.2.2/epubcheck.jar output/MyNewEbook.epub    # validace epub souboru
```

## Generate
```bash
# PDF
saxon -s:'all.xml' -xsl:xml-pdf.xsl -o:output/f.fo  && fop -fo 'output/f.fo' -pdf 'output/out.pdf'

# HTML pages
saxon all.xml xml-html.xsl > output/index.html   

#epub
sh epub-create.sh
```