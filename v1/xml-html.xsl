<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version="2.0">
    <xsl:output method="html" encoding="UTF-8" indent="yes"/>
    <xsl:variable name="ext" select="'.html'"/>
    <xsl:variable name="mode">html</xsl:variable>
    <xsl:include href="html-templates.xsl" />
</xsl:stylesheet>