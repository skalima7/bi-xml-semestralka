<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <xsl:output method="xml" indent="yes"/>

    <xsl:template match="/">
        <fo:root>
            <fo:layout-master-set>
                <fo:simple-page-master master-name="index" page-height="29.7cm" page-width="21.0cm" margin="2cm">
                    <fo:region-body margin-bottom="10mm" margin="25mm" margin-top="37mm"/>
                    <fo:region-after extent="8mm"/>
                </fo:simple-page-master>
                <fo:simple-page-master master-name="country" page-height="29.7cm" page-width="21.0cm" margin="2cm">
                    <fo:region-body margin-bottom="10mm"/>
                    <fo:region-after extent="8mm"/>
                </fo:simple-page-master>
                <fo:page-sequence-master master-name="my-sequence-master">
                    <fo:repeatable-page-master-reference master-reference="index" maximum-repeats="1"/>
                    <fo:repeatable-page-master-reference master-reference="country" maximum-repeats="1000"/>
                </fo:page-sequence-master>
            </fo:layout-master-set>
            <fo:page-sequence master-reference="my-sequence-master">
                <!-- Společný obsah všech stránek v zápatí stránky -->
                <fo:static-content flow-name="xsl-region-after">
                    <fo:block>
                        <xsl:text>Strana </xsl:text>
                        <fo:page-number/>
                        <xsl:text> z </xsl:text>
                        <fo:page-number-citation ref-id="last_page"/>
                    </fo:block>
                </fo:static-content>

                <fo:flow flow-name="xsl-region-body" font-family="Arial,Helvetica,sans-serif" font-size="11pt">
                    <fo:block>
                        <fo:block font-size="16pt" font-weight="bold" text-align="center">TABLE OF CONTENTS</fo:block>


                        <!-- 
                            country name
                                |-> One page summary
                                    |-> list item per section
                                |->travel facts
                            country name
                                |-> One page summary
                                    |-> list per section
                                |->travel facts
                            ...

                            It is complicated, because it have nested lists
                         -->
                        <xsl:for-each select="countries/country">
                            <xsl:variable name="countryFile" select="@id"/>
                            <fo:list-block provisional-distance-between-starts="1cm" provisional-label-separation="1cm">
                                <fo:list-item>
                                    <fo:list-item-label>
                                        <fo:block>

                                        </fo:block>
                                    </fo:list-item-label>
                                    <fo:list-item-body>
                                        <fo:block text-align-last="justify">
                                            <fo:basic-link internal-destination="{@id}" font-weight="bold">
                                                <xsl:value-of select="name"/>
                                            </fo:basic-link>
                                            <fo:leader leader-pattern="dots" />
                                            <fo:basic-link internal-destination="{@id}">
                                                <fo:page-number-citation ref-id="{@id}"/>
                                            </fo:basic-link>
                                        </fo:block>
                                    </fo:list-item-body>
                                </fo:list-item>
                                <fo:list-item>
                                    <fo:list-item-label>
                                        <fo:block>
                                        </fo:block>
                                    </fo:list-item-label>
                                    <fo:list-item-body>

                                        <fo:list-block margin-left="1cm">
                                            <fo:list-item>
                                                <fo:list-item-label>
                                                    <fo:block />
                                                </fo:list-item-label>
                                                <fo:list-item-body>
                                                    <fo:block text-align-last="justify">
                                                        <fo:basic-link internal-destination="{$countryFile}#{info/onePageSummary/local-name()}">
                                                        One page summary
                                                        </fo:basic-link>
                                                        <fo:leader leader-pattern="dots" />
                                                        <fo:basic-link internal-destination="{@id}">
                                                            <fo:page-number-citation ref-id="{@id}"/>
                                                        </fo:basic-link>
                                                    </fo:block>
                                                </fo:list-item-body>
                                            </fo:list-item>

                                            <fo:list-item>
                                                <fo:list-item-label>
                                                    <fo:block />
                                                </fo:list-item-label>
                                                <fo:list-item-body>
                                                    <fo:list-block margin-left="0.8cm">
                                                        <xsl:for-each select="info/onePageSummary/child::*">
                                                            <fo:list-item>
                                                                <fo:list-item-label>
                                                                    <fo:block />
                                                                </fo:list-item-label>
                                                                <fo:list-item-body>
                                                                    <fo:block text-align-last="justify">
                                                                        <fo:basic-link internal-destination="{$countryFile}#{local-name()}">
                                                                            <xsl:value-of select="local-name()" />
                                                                        </fo:basic-link>
                                                                        <fo:leader leader-pattern="dots" />
                                                                        <fo:basic-link internal-destination="{$countryFile}">
                                                                            <fo:page-number-citation ref-id="{$countryFile}"/>
                                                                        </fo:basic-link>
                                                                    </fo:block>
                                                                </fo:list-item-body>
                                                            </fo:list-item>
                                                        </xsl:for-each>
                                                    </fo:list-block>
                                                </fo:list-item-body>
                                            </fo:list-item>

                                            <fo:list-item>
                                                <fo:list-item-label>
                                                    <fo:block text-align-last="justify">
                                                        <fo:basic-link internal-destination="{$countryFile}#{info/travelFacts/local-name()}">
                                                    Travel facts
                                                        </fo:basic-link>
                                                        <fo:leader leader-pattern="dots" />
                                                        <fo:basic-link internal-destination="{@id}">
                                                            <fo:page-number-citation ref-id="{@id}"/>
                                                        </fo:basic-link>
                                                    </fo:block>
                                                </fo:list-item-label>
                                                <fo:list-item-body>
                                                    <fo:block />
                                                </fo:list-item-body>
                                            </fo:list-item>
                                        </fo:list-block>
                                    </fo:list-item-body>
                                </fo:list-item>
                            </fo:list-block>
                        </xsl:for-each>
                    </fo:block>
                    <xsl:apply-templates select="countries/country" />
                    <fo:block id="last_page"/>
                </fo:flow>
            </fo:page-sequence>
        </fo:root>
    </xsl:template>

    <xsl:template match="country">
        <fo:block page-break-before="always" id="{@id}">
            <fo:block font-size="22pt" font-weight="bold" text-align="center">
                <xsl:value-of select="name"/>
            </fo:block>

            <fo:block id="{@id}#{info/onePageSummary/local-name()}">
                <xsl:apply-templates select="info/onePageSummary">
                    <xsl:with-param name="id" select="@id"/>
                </xsl:apply-templates>
            </fo:block>
            <fo:block>
                <xsl:for-each select="images/image[not(contains(@alt, 'Flag'))]">
                    <fo:external-graphic src="{@src}" content-height="scale-to-fit" content-width="4cm"/>
                </xsl:for-each>
            </fo:block>
            <fo:block id="{@id}#{info/travelFacts/local-name()}" margin-top="1cm">
                <xsl:apply-templates select="info/travelFacts" />
            </fo:block>
        </fo:block>

        <!-- Right watermark -->
        <fo:block-container position="absolute" height="19.5cm" width="1.9cm" right="-2.1cm" top="-1.3cm" background-image="url('media/images/watermark.png')" background-position="left" background-color="transparent">
            <fo:block/>
        </fo:block-container>
    </xsl:template>

    <xsl:variable name="uppercase">ABCDEFGHIJKLMNOPQRSTUVWXYZ</xsl:variable>
    <xsl:variable name="lowercase">abcdefghijklmnopqrstuvwxyz</xsl:variable>

    <xsl:template match="government">
        <xsl:param name="id"/>
        <fo:inline-container width="47%">
            <fo:block >
                <xsl:call-template name="h2">
                    <xsl:with-param name="text">
                        <xsl:call-template name="Capitalize">
                            <xsl:with-param name="text" select="local-name()"/>
                        </xsl:call-template>
                    </xsl:with-param>
                    <xsl:with-param name="id" select="concat($id, '#', local-name())"/>
                </xsl:call-template>

                <fo:block>Chief of State:
                    <xsl:apply-templates select="chiefOfState" />
                </fo:block>

                <fo:block>Head of government:
                    <xsl:apply-templates select="headOfGovernment" />
                </fo:block>

                <fo:block>Capital: 
                    <xsl:apply-templates select="capital" />
                </fo:block>
                <fo:external-graphic src="{../../../images/image[contains(@alt, 'Flag')]/@src}" content-height="scale-to-fit" height="3cm" content-width="4cm"/>
            </fo:block>
        </fo:inline-container >
    </xsl:template>

    <xsl:template match="geography|economy|society">
        <xsl:param name="id"/>
        <fo:inline-container width="47%">
            <fo:block >
                <xsl:call-template name="h2">
                    <xsl:with-param name="text">
                        <xsl:call-template name="Capitalize">
                            <xsl:with-param name="text" select="local-name()"/>
                        </xsl:call-template>
                    </xsl:with-param>
                    <xsl:with-param name="id" select="concat($id, '#', local-name())"/>
                </xsl:call-template>

                <xsl:value-of select="overview" />

                <xsl:for-each select="*[not(name() = 'overview')]">
                    <fo:block keep-together.within-column="always">
                        <xsl:call-template name="h3">
                            <xsl:with-param name="text">
                                <xsl:call-template name="Capitalize">
                                    <xsl:with-param name="text" select="local-name()"/>
                                </xsl:call-template>
                            </xsl:with-param>
                            <xsl:with-param name="year" select="@year" />
                        </xsl:call-template>

                        <!-- IF -->
                        <xsl:for-each select="./* except (resource|climate|language|percentage)">
                            <fo:block>
                                <fo:inline font-style="italic">
                                    <xsl:value-of select="concat(local-name(), ': ')" />
                                </fo:inline>
                                <xsl:apply-templates select="./*" />
                            </fo:block>
                        </xsl:for-each>
                        <!-- ELSE -->
                        <xsl:for-each select="./(resource|climate|language|percentage)">
                            <xsl:if test="position() > 1">, </xsl:if>
                            <xsl:apply-templates select="."/>
                            <xsl:if test=".[@official='true']">
                                <xsl:text> (Official)</xsl:text>
                            </xsl:if>
                        </xsl:for-each>
                    </fo:block>
                </xsl:for-each>
            </fo:block>
        </fo:inline-container>
    </xsl:template>

    <xsl:template name="Capitalize">
        <xsl:param name="text" />
        <xsl:value-of select="concat(translate(substring($text, 1, 1), $lowercase, $uppercase), substring($text, 2))" />
    </xsl:template>

    <xsl:template name="h2">
        <xsl:param name="text" />
        <xsl:param name="id" />
        <fo:block font-size="18pt" id="{$id}" padding-top="10px" font-weight="700">
            <xsl:value-of select="$text" />
        </fo:block>
    </xsl:template>

    <xsl:template name="h3">
        <xsl:param name="text" />
        <xsl:param name="year" />
        <fo:block font-size="14pt" padding-top="8px">
            <xsl:value-of select="$text" />
            <xsl:if test="$year">
                <fo:inline font-size="7pt">
                    <xsl:value-of select="concat(' ', $year)" />
                </fo:inline>
            </xsl:if>
        </fo:block>
    </xsl:template>

    <xsl:template match="capital">

        <xsl:value-of select="name"/>
        <xsl:if test="note">
            <xsl:value-of select="concat(' (', note, ')')"/>
        </xsl:if>

    </xsl:template>

    <xsl:template match="price">
        <xsl:value-of select="concat(format-number(text(), '###,###'), ' ', @currency)"/>
    </xsl:template>

    <xsl:template match="partner">
        <xsl:if test="position() > 1">, </xsl:if>
        <xsl:value-of select="name"/>
        <xsl:text> (</xsl:text>
        <xsl:apply-templates select="percentage" />
        <xsl:text>)</xsl:text>
    </xsl:template>

    <xsl:template match="person">
        <xsl:value-of select="concat(title, ' ', name)"/>
    </xsl:template>

    <xsl:template match="overview">
        <fo:block>
            <xsl:value-of select="."/>
        </fo:block>
    </xsl:template>

    <xsl:template match="percentage">
        <xsl:value-of select="concat(., ' %')"/>
    </xsl:template>


    <xsl:template match="sqkm">
        <xsl:value-of select="concat(text(), ' sq km')"/>
    </xsl:template>

    <xsl:template match="travelFacts">
        <fo:block keep-together.within-column="always">
            <xsl:call-template name="h2">
                <xsl:with-param name="text">
                Travel facts
                </xsl:with-param>
            </xsl:call-template>

            <fo:table table-layout="fixed">
                <fo:table-header font-weight="700" background-color="#39a2fb">
                    <fo:table-row>
                        <fo:table-cell>
                            <fo:block>Section</fo:block>
                        </fo:table-cell>
                        <fo:table-cell>
                            <fo:block>Description</fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                </fo:table-header>
                <fo:table-body>
                    <xsl:for-each select="fact">
                        <fo:table-row border-bottom="1px solid #ccc">
                            <fo:table-cell>
                                <fo:block margin-top="5px">
                                    <xsl:value-of select="name"/>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell>
                                <fo:block margin-bottom="10px" margin-top="5px">
                                    <xsl:value-of select="text"/>
                                    <xsl:text>&#160;</xsl:text>
                                    <xsl:apply-templates select="link" />
                                </fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                    </xsl:for-each>
                </fo:table-body>
            </fo:table>
        </fo:block>

        <fo:block margin-top="30px" text-align="center">
            <fo:block>
                <xsl:value-of select="footer/name"/>
            </fo:block>
            <xsl:apply-templates select="footer/(link|updated)"/>
        </fo:block>
    </xsl:template>

    <xsl:template match="link">
        <xsl:choose>
            <xsl:when test="string(.) != ''">
                <fo:basic-link external-destination="url('{@url}')" color="blue" text-decoration="underline">
                    <xsl:value-of select="." />
                </fo:basic-link>
            </xsl:when>
            <xsl:otherwise>
                <fo:basic-link external-destination="url('{@url}')" color="blue" text-decoration="underline">
                    <xsl:value-of select="@url" />
                </fo:basic-link>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="updated">
        <fo:block margin-top="10px" color="grey">
            <xsl:value-of select="concat('Last updated ', format-date(.,'[M01]/[D01]/[Y0001]'))" />
        </fo:block>
    </xsl:template>

</xsl:stylesheet>
