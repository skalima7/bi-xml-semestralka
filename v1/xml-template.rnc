datatypes xs = "http://www.w3.org/2001/XMLSchema-datatypes"
namespace a = "http://relaxng.org/ns/compatibility/annotations/1.0"


start = Countries

person = element person {
            element title { xs:string },
            element name { xs:string }
}
sqkm = element sqkm { xs:double{
        minInclusive = "0"
} }
price = element price {
    attribute currency { "USD"|"CZK" },
    xs:string 
}
percentage = element percentage { xs:double{
    minInclusive = "0"
    maxInclusive = "100"
} }
partners = element partners {
                  element partner {
                      element name {  xs:string  },
                      percentage
                  }*
              }
year_attr = attribute year {xs:positiveInteger{
    maxInclusive = "9999"
    minInclusive = "1000"
}}

link = element link {
        attribute url {xs:anyURI},
        xs:string
}


Countries = element countries {
  element country {
      attribute id {xs:ID{
          pattern = "C_[a-z]+"
      }},
    element name { xs:string },
    element info {
        element onePageSummary {
            element government {
                element chiefOfState {
                    person
                },
                element headOfGovernment {
                    person
                },
                element capital {
                    element name {xs:string},
                    element note {xs:string{
                        pattern = ".+"
                    }}?
                },
                element legislature { xs:string }
            },
            element geography {
                    element area {
                        element total { sqkm },
                        element land { sqkm },
                        element water { sqkm }
                    },
                    element climates {
                        element climate {  xs:string }+
                    },
                    element resources {
                        element resource { xs:string }+
                    }
            },
            element economy {
                    element overview { xs:string },
                    element exports {
                        year_attr,
                        element total { price },
                        partners
                    },
                    element imports {
                        year_attr,
                        element total { price },
                        partners
                    }
            },
            element society {
                element population {
                    year_attr,
                    element growth { percentage },
                    element total {
                        element value { xs:double },
                        element unit { xs:string }
                    }
                },
                element languages {
                    year_attr?,
                    element language {
                        [ a:defaultValue = "true" ] attribute official { "true" | "false"}?,

                        element name {  xs:string  },
                        percentage?
                    }+
                },
                element literacy {
                    year_attr,
                    percentage
                }?
            }
        },
        element travelFacts {
            element fact {
                element name { xs:string },
                element text { xs:string },
                link?
            }*,
            element footer {
                element name { xs:string },
                link*,
                element updated { xs:date }?
            }
        }
    },
    element images {
        element image {
            attribute src { xs:anyURI },
            attribute alt { xs:string }
        }+
    }
  }+
}