<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version="2.0">

    <xsl:template match="/">
        <xsl:call-template name="xhtml-header"/>
        <html>
            <head>
                <title>Countries</title>
                <link rel="stylesheet" href="media/css/styles.css" />
                <link rel="stylesheet" href="media/css/panda.css" />
            </head>
            <body>
                <div class="index">
                    <div class="panda">
                        <div class="ears">
                            <div class="leftEar" />
                            <div class="rightEar" />
                        </div>
                        <div class="head">
                            <div class="eyes">
                                <div class="leftEye">
                                    <div class="irisEye" />
                                </div>
                                <div class="rightEye">
                                    <div class="irisEye" />
                                </div>
                            </div>
                            <div class="face">
                                <div class="nose">
                                    <div class="lineDown" />
                                </div>
                                <div class="mouth">
                                    <div class="line" />
                                </div>
                            </div>
                        </div>
                        <!-- <div class="hands" /> -->
                    </div>
                    <h1>Content:</h1>
                    <xsl:for-each select="countries/country">
                        <xsl:variable name="countryFile" select="concat('country', position(), $ext)"/>
                        <ul>
                            <li>
                                <a href="{$countryFile}">
                                    <xsl:value-of select="name"/>
                                </a>
                                <ul>
                                    <li class="lh">
                                        <a href="{$countryFile}#{info/onePageSummary/local-name()}">One page summary</a>
                                    </li>
                                    <xsl:for-each select="info/onePageSummary/child::*">
                                        <li>
                                            <a href="{$countryFile}#{local-name()}">
                                                <xsl:value-of select="local-name()" />
                                            </a>
                                        </li>
                                    </xsl:for-each>
                                    <li class="lh">
                                        <a href="{$countryFile}#{info/travelFacts/local-name()}">Travel facts</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </xsl:for-each>
                    <xsl:apply-templates select="countries/country" />
                </div>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="country">
        <xsl:result-document href="output/country{position()}{$ext}">
            <xsl:call-template name="xhtml-header"/>
            <html>
                <head>
                    <title>
                        <xsl:value-of select="./name"/>
                    </title>
                    <link rel="stylesheet" href="media/css/styles.css" />
                </head>
                <body>
                    <h1 class="header">
                        <xsl:value-of select="./name"/>
                    </h1>
                    <div class="onePageSummary" id="{info/onePageSummary/local-name()}">
                        <xsl:apply-templates select="info/onePageSummary" />
                    </div>
                    <div class="images">
                        <xsl:for-each select="images/image[not(contains(@alt, 'Flag'))]">
                            <img src="{@src}" alt="{@alt}"/>
                        </xsl:for-each>
                    </div>
                    <div class="travelFacts" id="{info/travelFacts/local-name()}">
                        <xsl:apply-templates select="info/travelFacts" />
                    </div>
                </body>
            </html>
        </xsl:result-document>
    </xsl:template>

    <xsl:variable name="uppercase">ABCDEFGHIJKLMNOPQRSTUVWXYZ</xsl:variable>
    <xsl:variable name="lowercase">abcdefghijklmnopqrstuvwxyz</xsl:variable>

    <xsl:template match="government">
        <div>
            <div>
                <h2 id="{local-name()}">
                    <xsl:call-template name="Capitalize">
                        <xsl:with-param name="text" select="local-name()"/>
                    </xsl:call-template>
                </h2>
                <span class="label">Chief of State: </span>
                <xsl:apply-templates select="chiefOfState" />
                <br/>
                <span class="label">Head of government: </span>
                <xsl:apply-templates select="headOfGovernment" />
                <br/>

                <span class="label">Capital: </span>
                <xsl:apply-templates select="capital" />
            </div>
            <img src="{../../../images/image[contains(@alt, 'Flag')]/@src}" alt="{images/image[1]/@alt}"/>
        </div>
    </xsl:template>

    <xsl:template match="geography|economy|society">
        <div>
            <h2 id="{local-name()}">
                <xsl:call-template name="Capitalize">
                    <xsl:with-param name="text" select="local-name()"/>
                </xsl:call-template>
            </h2>
            <xsl:value-of select="overview" />

            <xsl:for-each select="*[not(name() = 'overview')]">
                <h3>
                    <xsl:call-template name="Capitalize">
                        <xsl:with-param name="text" select="local-name()"/>
                    </xsl:call-template>
                    <xsl:if test="@year">
                        <span class="year">
                            <xsl:value-of select="concat(' ', @year)" />
                        </span>
                    </xsl:if>
                </h3>

                <xsl:for-each select="./* except (resource|climate|language|percentage)">
                    <span class="label">
                        <xsl:value-of select="concat(local-name(), ': ')" />
                    </span>
                    <xsl:apply-templates select="./*" />
                    <br/>
                </xsl:for-each>

                <xsl:for-each select="./(resource|climate|language|percentage)">
                    <!-- <span> -->
                    <xsl:if test="position() > 1">, </xsl:if>
                    <xsl:apply-templates select="."/>
                    <xsl:if test=".[@official='true']">
                        <xsl:text> (Official)</xsl:text>
                    </xsl:if>
                    <!-- </span> -->
                </xsl:for-each>
            </xsl:for-each>
        </div>

    </xsl:template>

    <xsl:template name="Capitalize">
        <xsl:param name="text" />
        <xsl:value-of select="concat(translate(substring($text, 1, 1), $lowercase, $uppercase), substring($text, 2))" />
    </xsl:template>

    <xsl:template match="capital">
        <span>
            <xsl:value-of select="name"/>
            <xsl:if test="note">
                <xsl:value-of select="concat(' (', note, ')')"/>
            </xsl:if>
        </span>
    </xsl:template>

    <xsl:template match="price">
        <span>
            <xsl:value-of select="concat(format-number(text(), '###,###'), ' ', @currency)"/>
        </span>
    </xsl:template>

    <xsl:template match="partner">
        <xsl:if test="position() > 1">, </xsl:if>
        <xsl:value-of select="name"/>
        <xsl:text> (</xsl:text>
        <xsl:apply-templates select="percentage" />
        <xsl:text>)</xsl:text>
    </xsl:template>

    <xsl:template match="person">
        <span>
            <xsl:value-of select="concat(title, ' ', name)"/>
        </span>
    </xsl:template>

    <xsl:template match="overview">
        <p>
            <xsl:value-of select="."/>
        </p>
    </xsl:template>

    <xsl:template match="percentage">
        <span>
            <xsl:value-of select="concat(., ' %')"/>
        </span>
    </xsl:template>


    <xsl:template match="sqkm">
        <span>
            <xsl:value-of select="concat(text(), ' sq km')"/>
        </span>
    </xsl:template>

    <xsl:template match="name">
        <span>
            <xsl:value-of select="."/>
        </span>
    </xsl:template>

    <xsl:template match="travelFacts">
        <h2>Travel facts</h2>
        <table>
            <tr>
                <th>Section</th>
                <th>Description</th>
            </tr>
            <xsl:for-each select="fact">
                <tr>
                    <td>
                        <xsl:value-of select="name"/>
                    </td>
                    <td>
                        <xsl:value-of select="text"/>
                        <xsl:text>&#160;</xsl:text>
                        <xsl:apply-templates select="link" />
                    </td>
                </tr>
            </xsl:for-each>
        </table>

        <div class="footer">
            <p>
                <xsl:value-of select="footer/name"/>
            </p>
            <xsl:apply-templates select="footer/(link|updated)"/>
        </div>
    </xsl:template>

    <xsl:template match="link">
        <xsl:choose>
            <xsl:when test="string(.) != ''">
                <a href="{@url}">
                    <xsl:value-of select="." />
                </a>
            </xsl:when>
            <xsl:otherwise>
                <a href="{@url}">
                    <xsl:value-of select="@url" />
                </a>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="updated">
        <p>
            <xsl:value-of select="concat('Last updated ', format-date(.,'[M01]/[D01]/[Y0001]'))" />
        </p>
    </xsl:template>

    <xsl:template name="xhtml-header">
        <!-- <xsl:if test="$mode = 'xhtml'">
            <?xml version="1.0" encoding="utf-8"?>
            <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
        </xsl:if> -->
    </xsl:template>
</xsl:stylesheet>
